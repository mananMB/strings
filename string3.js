// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

const monthRange = { start: 1, end: 12 };
const dateRange = { start: 1, end: 31 };
const monthArray = {
  1: "January",
  2: "February",
  3: "March",
  4: "April",
  5: "May",
  6: "June",
  7: "July",
  8: "August",
  9: "September",
  10: "October",
  11: "November",
  12: "December",
};

const string3 = (date) => {
  const splitDate = (date) => {
    return date.split("/");
  };

  const getMonth = (date) => {
    return splitDate(date)[1];
  };

  const isValidDate = (date) => {
    if (date === undefined) {
      return false;
    }

    const splitDateArray = splitDate(date);

    if (splitDateArray.length > 3) {
      return false;
    }

    for (let i = 0; i < 3; i++) {
      switch (i) {
        case 0:
          if (
            isNaN(splitDateArray[i]) ||
            splitDateArray[i] < dateRange.start ||
            splitDateArray[i] > dateRange.end
          ) {
            return false;
          }
          break;
        case 1:
          if (
            isNaN(splitDateArray[i]) ||
            splitDateArray[i] < monthRange.start ||
            splitDateArray[i] > monthRange.end
          ) {
            return false;
          }
          break;
        case 2:
          if (isNaN(splitDateArray[i])) {
            return false;
          }
      }
    }
    return true;
  };

  if (isValidDate(date)) {
    return monthArray[getMonth(date)];
  }

  return [];
};

module.exports = string3;
