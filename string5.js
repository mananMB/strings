// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

const string5 = (array) => {
  if (array.length !== 0) {
    return array.join(' ');
  }
  return [];
};

module.exports = string5;
