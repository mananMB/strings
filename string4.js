// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

const twoNameKeys = ["first_name", "last_name"];
const threeNameKeys = ["first_name", "middle_name", "last_name"];
const string4 = (nameObject) => {
  const isValidNameObject = (nameObject) => {
    if (
      JSON.stringify(Object.keys(nameObject)) === JSON.stringify(threeNameKeys)
    ) {
      return true;
    }

    return (
      JSON.stringify(Object.keys(nameObject)) === JSON.stringify(twoNameKeys)
    );
  };

  const createArrayOfValues = (nameObject) => {
    let array = [];
    for (let i in nameObject) {
      array.push(nameObject[i]);
    }
    return array;
  };

  const convertArrayValuesToLowerCase = (array) => {
    return array.map((element) => {
      return element.toLowerCase();
    });
  };

  const convertArrayWordsToCapitalCase = (array) => {
    return array.map((element, index) => {
      return element.slice(0, 1).toUpperCase() + element.slice(1);
    });
  };
  const createStringFromArray = (array) => {
    return array.join(" ");
  };

  const convertToCapitalCaseString = (nameObject) => {
    const arrayOfObjectValues = createArrayOfValues(nameObject);
    const nameArray = convertArrayValuesToLowerCase(arrayOfObjectValues);
    const capitalCaseArray = convertArrayWordsToCapitalCase(nameArray);
    return createStringFromArray(capitalCaseArray);
  };

  if (isValidNameObject(nameObject)) {
    return convertToCapitalCaseString(nameObject);
  }

  return [];
};

module.exports = string4;
