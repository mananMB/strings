const string5 = require("../string5");
const string3 = require("../string3");
const expect = require("chai").expect;

const testArray = ["the", "quick", "brown", "fox"];

const result = string5(testArray);

console.log(result);

describe("Test String #5", () => {
  it("returns joined string when array is not empty", () => {
    const result = string5(testArray);
    expect(result).to.equal("the quick brown fox");
  });
  it("returns [] when array is empty", () => {
    const result = string5([]);
    expect(result).to.deep.equal([]);
  });
  it("returns [] when array is not provided", () => {
    const result = string3();
    expect(result).to.deep.equal([]);
  });
});
