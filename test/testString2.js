const string2 = require("../string2");
const expect = require("chai").expect;

const testString = "111.111.11.35";
const expectedResult = [111, 111, 11, 35];

describe("Test String #1", () => {
  it("returns array of ip components separated by delimiter", () => {
    const result = string2(testString);
    expect(result).to.deep.equal(expectedResult);
  });
  it("returns [] when IP provided contains characters other than Numbers and .", () => {
    const result = string2("hello.12.111.267");
    expect(result).to.deep.equal([]);
  });
  it("returns [] when numbers in IP are no 0 < Numbers in IP < 255", () => {
    const result = string2("111.200.50.260");
    expect(result).to.deep.equal([]);
  });
  it("returns [] when IP contains more than 4 delimiters", () => {
    const result = string2("111.200.50.260.60");
    expect(result).to.deep.equal([]);
  });
  it("returns [] when IP is not provided", () => {
    const result = string2();
    expect(result).to.deep.equal([]);
  });
});
