const string1 = require("../string1");
const expect = require("chai").expect;

describe("Test String #1", () => {
  it("returns {number} when string is a positive number", () => {
    const result = string1("$1,123.45");
    expect(result).to.equal("1123.45");
  });
  it("returns 0 when string is not a number", () => {
    const result = string1("$1,10s.12");
    expect(result).to.equal(0);
  });
  it("returns -{number} when string is a negative number", () => {
    const result = string1("-$1,352.24");
    expect(result).to.equal("-1352.24");
  });
});
