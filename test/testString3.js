const string3 = require("../string3");
const expect = require("chai").expect;

const testString = "20/12/2022";

describe("Test String #3", () => {
  it("returns month when valid date is provided", () => {
    const result = string3("20/12/2022");
    expect(result).to.equal("December");
  });
  it("returns [] when argument is not provided", () => {
    const result = string3();
    expect(result).to.deep.equal([]);
  });
  it("returns [] when date is not valid (eg. 12/12/2001/12)", () => {
    const result = string3("12/12/2001/12");
    expect(result).to.deep.equal([]);
  });
  it("returns [] when date day is out of range", () => {
    const result = string3("32/13/2001");
    expect(result).to.deep.equal([]);
  });
  it("returns [] when date month is out of range", () => {
    const result = string3("12/13/2001");
    expect(result).to.deep.equal([]);
  });
  it("returns [] when date year is NaN", () => {
    const result = string3("12/12/2asd");
    expect(result).to.deep.equal([]);
  });
});
