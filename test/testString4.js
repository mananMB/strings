const string4 = require("../string4");
const expect = require("chai").expect;

const testObjectThreeValidKeys = {
  first_name: "JoHN",
  middle_name: "doe",
  last_name: "SMith",
};

const testObjectTwoValidKeys = {
  first_name: "JoHN",
  last_name: "SMith",
};

describe("Test String #4", () => {
  it("returns title case string when valid two key object is provided", () => {
    const result = string4(testObjectTwoValidKeys);
    expect(result).to.equal("John Smith");
  });
  it("returns title case string when valid three key object is provided", () => {
    const result = string4(testObjectThreeValidKeys);
    expect(result).to.equal("John Doe Smith");
  });
  it("returns [] when invalid name object is provided", () => {
    const result = string4({ first_name: "John", name: "Bro" });
    expect(result).to.deep.equal([]);
  });
});
