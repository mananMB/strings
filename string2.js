// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143".
// Split it into its component parts 111, 139, 161, 143 and
// return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.

const string2 = (IP) => {
  const splitIPAddress = (IP) => {
    return IP.split(".");
  };

  const convertStringToNumber = (IPArray) => {
    return IPArray.map((element) => {
      return Number(element);
    });
  };
  const isValidIP = (IP) => {
    if (IP === undefined || IP === "0.0.0.0") {
      return false;
    }
    const splitIP = splitIPAddress(IP);
    if (splitIP.length > 4) {
      return false;
    }

    for (let i = 0; i < 4; i++) {
      if (isNaN(splitIP[i]) || splitIP[i] < 0 || splitIP[i] > 255) {
        return false;
      }
    }

    return true;
  };

  if (isValidIP(IP)) {
    return convertStringToNumber(splitIPAddress(IP));
  }

  return [];
};

module.exports = string2;
